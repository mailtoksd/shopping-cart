<div class="center-wrapper sitemap-pages-main">

<div ><h1 style="margin-top: 2%; margin-bottom: 10px;font-family: 'Open Sans'; font-size: 24px; line-height: 30px; font-weight: 900; color: #f99d1c; padding-left: 2%;"><?php echo Yii::t('page','About Majju Honey')?></h1></div>
<form name="AboutUsForm" method="post">
<div class="col-md-12">

<div class="row controls">

	<div class="col-md-6 ">
		<div class="image-boxes-img-wrapper">
			<img class="image-boxes-img" style="box-shadow:0 1px 1px rgba(0,0,0,.22); width:80%" src="https://www.honeyhouse.in/wp-content/uploads/2015/12/quality.jpg" alt="" title=""></div>
	</div>
	<div class="col-md-6 aboutdiv">
		Honey House is a combined effort of Majju Honey and HashTag Technologies.</br>
</br>		Mr. C. Jeykumar of Majju Honey has a national record in Limca Book of Records for covering his entire body with 60000 Italian bees for 24 hours. He also kept 175 bees in his closed mouth for 3 minutes 7 seconds. His passion combined with the vision of Hashtag Technologies has resulted in forming Honey House which stands for pure and natural honey and related products.</br></br>
 Quality and trust are our core values. Our motto is to assure high quality, natural and pure honey prepared in hygienic environments and measured through strong quality controls. Our products are available online which depicts a wide range of customers who can make the most of our products around the world.
	</div>
</div>
</form>
  <div class="clearfix"></div>
   </div>
