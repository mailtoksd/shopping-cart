<div class="center-wrapper sitemap-pages-main">

<div ><h1 style="margin-top: 2%; margin-bottom: 10px;font-family: 'Open Sans'; font-size: 24px; line-height: 30px; font-weight: 900; color: #f99d1c; padding-left: 2%;"><?php echo Yii::t('page','Quality Policy')?></h1></div>
<form name="Quality Policy Form" method="post">
<div class="col-md-12">

<div class="row controls">

	<div class="col-md-6 ">
		<div class="image-boxes-img-wrapper">
			<img class="image-boxes-img" style="box-shadow:0 5px 5px 5px rgba(0,0,0,.22); width:80%" src="https://honey.com/images/cropped/jar_inset_small.jpg#asset:95008" alt="" title=""></div>
	</div>
	<div class="col-md-6 aboutdiv">

		We take pride in being a quality conscious company that offers the best products to all our customers. Our detailed Quality policy has been laid in such a way to ensure that quality is strictly adhered to at various levels of our functions. Honey adulteration has become a prevalent concern for most of the consumers. Honey is mixed with several inexpensive sweeteners such as sugar syrups, jaggery and molasses inverted by acids or enzymes from corn, sugar cane, sugar beet and syrups of natural origin such as maple. Adulteration of pure honey with synthetic honey is an issue to be seriously considered.<br><br>

These adulteration techniques makes the honey appear lighter in colour and pure. However, apart from having no nutritional value these may affect the consumer’s health in the long-term.<br></br>

At Honey House, equal care is taken at every stage of honey extraction, processing and packaging as we understand the importance of delivering chemical free and totally natural products. For us, nothing comes before our customer’s belief in our products. Our quality experts take complete onus to constantly monitor the several processes at all times so that our customers get to relish the best of our products. Each product of ours is 100% pure with all the health benefits that comes with natural honey and related goods.
	</div>
</div>
<div class="row controls">
	<div class="col-md-6 aboutdiv">
			<h1>How do we select the hive placement regions?</h1>
		We have strict criteria to select hive placements. These include:

Proximity of Natural Water sources: Bees require water as much as humans to thrive. In order to not use any antibiotics and for natural bee health, we choose regions which are in close proximity to natural water sources.

Organic Fields: Pesticides and Artificial manure affect the bee health in a big way. We choose farms which use organic feed and natural pesticides.

No mobile towers: Microwaves adversely impact pollination and the health of bees. So we choose farms which are away from mobile phone towers.

Constant Migration: Since India is a huge country with a variety of flora, the flowering season of each region keeps changing, and we need to migrate the honey colonies at strategic intervals to keep the bee population on the rise and also improve the quality of honey.
	</div>
	<div class="col-md-6 aboutdiv">
			<h1>How do we select the hive placement regions?</h1>
		We have strict criteria to select hive placements. These include:

Proximity of Natural Water sources: Bees require water as much as humans to thrive. In order to not use any antibiotics and for natural bee health, we choose regions which are in close proximity to natural water sources.

Organic Fields: Pesticides and Artificial manure affect the bee health in a big way. We choose farms which use organic feed and natural pesticides.

No mobile towers: Microwaves adversely impact pollination and the health of bees. So we choose farms which are away from mobile phone towers.

Constant Migration: Since India is a huge country with a variety of flora, the flowering season of each region keeps changing, and we need to migrate the honey colonies at strategic intervals to keep the bee population on the rise and also improve the quality of honey.
	</div>
</div>
</form>
  <div class="clearfix"></div>
   </div>
