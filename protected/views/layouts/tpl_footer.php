<section id="bottom" >
    <div class="main-footer-1 top-footer" style="background-color:#f79b00;font-color: #333333;">
        <div class="container">
            <div class="col-md-4"> </div>
            <div class="col-md-8">
                <div class="row col-md-12 sp-os">
                    <div class="module-main-div">
                        <div class="cms-menu-links  top-nav">
                            <div class="module-content">
                                <div class="module-main-div">
                                    <?php
                                    $this->widget('zii.widgets.CMenu',
                                            array(
                                            'htmlOptions' => array('class' => 'nav navbar-nav top-nav'),
                                            'submenuHtmlOptions' => array('class' => 'dropdown-menu'),
                                            'itemCssClass' => 'item-test',
                                            'encodeLabel' => false,
                                            'items' => array(
                                            array('label' => Yii::t('common','text_termsofservice'),'url' => $this->createUrl('page/content',array('id_page'=>13))),
                                            array('label' => Yii::t('common','text_privacypolicy'),'url' =>$this->createUrl('page/content',array('id_page'=>12))),
                                            array('label' => Yii::t('common','text_refundpolicy'), 'url' => $this->createUrl('page/content',array('id_page'=>11))),
                                        ),
                                    ));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            </div>
        </div>
    </div>
    <div class="main-footer-2 mid-footer" style="background-color:#f79b00">
        <div class="container">
            <div class="row boxs-footer">
                <div class="col-md-6 box-footer">
                    <div class="row col-md-12 sp-os">
                        <div class="module-main-div">
                            <div class="follow-us-module-wapper">
                                <div class="heading-box"><h2><?php echo Yii::t('common','text_followus')?></h2></div>
                                <div class="module-content">
                                    <div class="follow-module">
                                        <div class="icon pull-left"><a href="" target="_blank" class="glyphicon spo-icon footer-icon glyphicon-fb-icon" ></a></div>
                                        <div class="icon pull-left"><a href="" target="_blank" class="glyphicon spo-icon footer-icon glyphicon-tw-icon" ></a></div>
                                        <div class="icon pull-left"><a href="" target="_blank" class="glyphicon spo-icon footer-icon glyphicon-v-icon" ></a></div>
                                        <div class="icon pull-left"><a href="" target="_blank" class="glyphicon spo-icon footer-icon glyphicon-yu-icon" ></a></div>
                                        <div class="icon pull-left"><a href="" target="_blank" class="glyphicon spo-icon footer-icon glyphicon-p-icon" ></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12 sp-os">
                        <div class="module-main-div">
                            <div class="icons-list-wapper">
                                <div class="module-content">
                                    <div class="icon-list-main-div">
                                        <div class="shipping-icon"><span class="glyphicon spo-icon icon-list-icon glyphicon-free-shipping-icon" ></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 box-footer">
                    <div class="row col-md-12 sp-os">
                        <div class="module-main-div">
                            <div class="contact-box">
                                <div class="heading-box"><h2><?php echo Yii::t('common','text_contact')?></h2></div>
                                <div class="module-content">
                                    <div class="contact-module">
										<p> <?php echo Yii::app()->config->getData('CONFIG_STORE_ADDRESS'); ?></p>
                                        <p><span class="glyphicon glyphicon-earphone"></span> <?php echo Yii::app()->config->getData('CONFIG_STORE_TELEPHONE_NUMBER'); ?></p>
                                        <p><span class="glyphicon glyphicon-envelope"></span> <?php echo Yii::app()->config->getData('CONFIG_STORE_SUPPORT_EMAIL_ADDRESS'); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /bottom-->

<footer>
    <div class="footer">
        <div class="container">
            <div class="row copyright_div">
                <p class="pull-left copyright">&copy; <?php echo Yii::app()->config->getData('CONFIG_WEBSITE_COPYRIGHTS'); ?></p>
            <!--  <p class=" pull-right col-md-2"> <a href="http://cartnex.org/" class="poweed_by_main" target="_blank"><?php  //echo Yii::t('common','text_poweredby');?> <i class="glyphicon spo-icon poweed_by_main-iconn"></i></a> </p> -->

            </div>
        </div>
    </div>
</footer>
</body>
</html>
