<?php

class Customer extends CActiveRecord
{   public $rememberMe;
    public $confirm;
	public function tableName()
	{
		return '{{customer}}';
	}

	public function rules()
	{
		return array(
			array('firstname,lastname,email,gender,telephone,newsletter,approved','required','on'=>'insert'),
			array('firstname,lastname,email,gender,telephone,newsletter,approved','required','on'=>'update'),
			array('email','required','on'=>'forgotpassword'),
            array('email', 'email'),
			array('email','unique'),
			array('password, confirm', 'length', 'min'=>6),
			array('password, confirm', 'required','on'=>'insert'),
			array('confirm', 'compare', 'compareAttribute'=>'password'),
			array('telephone', 'numerical', 'integerOnly'=>true),
			array('telephone', 'length', 'max'=>12),
			array('id_customer_address_default, id_customer_group, status, approved', 'numerical', 'integerOnly'=>true),
			array('gender, newsletter', 'length', 'max'=>1),
			array('status,approved','default','value'=>1,'setOnEmpty'=>true,'on'=>'insert'),
			array('date_created','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>true,'on'=>'insert'),
			array('date_created','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			array('cart, wishlist, date_created', 'safe'),
			array('id_customer, gender, firstname, lastname, dob, email, id_customer_address_default, id_customer_group, ip, telephone, cart, wishlist, fax, password, newsletter, status, approved, date_created', 'safe', 'on'=>'search'),
		);
	}


	public function attributeLabels()
	{
		return array(
			'approved' => 'Agreement',
			);
	}
	
	public function validatePassword($password)
	{
		return CPasswordHelper::verifyPassword($password,$this->password);
	}


	public function hashPassword($password)
	{
		return CPasswordHelper::hashPassword($password);
	}
	
	public function randomPassword()
	{	
		$length = 6;
		$chars = array_merge(range(0,9), range('a','z'), range('A','Z'));
		shuffle($chars);
		$password = implode(array_slice($chars, 0, $length));
		return $password;
	}


	public static function model($className=__CLASS__)
	{
            if((int)$_SESSION['user_id'])
            {
                Yii::app()->db->createCommand()->update('{{customer}}',array('ip'=>$_SERVER['REMOTE_ADDR'],'cart'=>isset($_SESSION['cart']) ? serialize($_SESSION['cart']) : '','wishlist'=>isset($_SESSION['wishlist']) ? serialize($_SESSION['wishlist']) : ''),'id_customer=:id',array(':id'=>(int)$_SESSION['user_id']));
            }
		return parent::model($className);
	}
	
	public function accountsDetails(){
		$connection = Yii::app()->db;
		/*$command = $connection->createCommand('select c.newsletter,c.firstname,c.lastname,c.telephone,c.email,ca.address_1,ca.city,s.name as sname,co.name as cname from {{customer}} c join {{customer_address}} ca on ca.id_customer=c.id_customer join r_state s on s.id_state=ca.id_state join r_country co on co.id_country=ca.id_country where c.id_customer='.Yii::app()->session['user_id']);*/
		$command=$connection->createCommand('select c.*,ca.firstname as address_firstname,ca.lastname as address_lastname,ca.telephone as address_telephone,ca.company as address_company,ca.address_1,ca.address_2,ca.postcode as address_postcode,ca.city as address_city,s.name as state ,co.name as country from {{customer}} c inner join {{customer_address}} ca on c.id_customer_address_default=ca.id_customer_address left join {{state}} s on ca.id_state=s.id_state left join {{country}} co on co.id_country=ca.id_country where c.id_customer="'.$_SESSION['user_id'].'"');
		
		return $command->queryRow();
	}

	public function login($input)
	{
		$user = Customer::model()->find(array("condition" => "email=:email","params" => array(":email" => $input['email'])));
	
		if(CPasswordHelper::verifyPassword($input['password'],$user->password) && $user!=null)
		{
			Yii::app()->session['user_first_name'] = $user->firstname;
			Yii::app()->session['user_id'] = $user->id_customer;
			Yii::app()->session['user_last_name'] = $user->lastname;
			Yii::app()->session['user_email'] = $user->email;
			Yii::app()->session['user_telephone'] = $user->telephone;
			Yii::app()->session['user_customer_group_id'] = $user->id_customer_group;
			Yii::app()->session['user_id_customer_address_default'] = $user->id_customer_address_default;
			$addwishlist = Customer::model()->find('id_customer="'.Yii::app()->session['user_id'].'"');
			$addwishlist->wishlist = serialize(Yii::app()->session['user_whishlist']);
			$addwishlist->cart = serialize($_SESSION['products']);
			$addwishlist->update(array('wishlist', 'cart'));
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getAddresses(){
		$customer_details=Yii::app()->db->createCommand("select *,s.name as statename,co.name as countryname from {{customer_address}} c left join {{state}} s on s.id_state=c.id_state left join {{country}} co on co.id_country=c.id_country where id_customer='".Yii::app()->session['user_id']."'");
		return $customer_details->queryAll();
	}

	public function getAddress($id){
		$customer_details=Yii::app()->db->createCommand("select  firstname,lastname,telephone,company,address_1,address_2,city,id_state,id_country,postcode from {{customer_address}} where id_customer_address='".$id."'");
		return $customer_details->queryRow();
	}
	
	public function getCustomerGroup($id)
	{
		$languages=Yii::app()->config->getData('languages');
		$id_language=$languages[Yii::app()->session['language']]['id_language'];
		$customer_group_details=Yii::app()->db->createCommand("select * from {{customer_group_description}} where id_language='".$id_language."' and id_customer_group='".(int)$id."'");
		return $customer_group_details->queryRow();
	}
}
