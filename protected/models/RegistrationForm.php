<?php

class RegistrationForm extends CFormModel
{
	public $firstname;
	public $lastname;
	public $telephone;
	public $password;
	public $confirm;
	public $gender;
	public $newsletter;
	public $email;
	public $acknowledgement;
	public $age;

	public function rules()
	{
		$required='telephone,firstname,lastname,newsletter,gender,email,password,confirm,age';
		if(Yii::app()->config->getData('CONFIG_STORE_ACCOUNT_TERMS'))
		{
			$required.=',acknowledgement';
		}

		return array(
			// name, email, subject and body are required
			array($required, 'required'),
			array('password, confirm', 'length', 'min'=>6),
			array('confirm', 'compare', 'compareAttribute'=>'password'),
			array('telephone', 'numerical', 'integerOnly'=>true),
			array('telephone', 'length', 'max'=>12),
			array('email', 'email'),
			array('email', 'unique', 'attributeName'=> 'email', 'className'=>'Customer'),
			//array('email','unique'),
		);
	}

	public function attributeLabels()
	{
		return array(
		);
	}
}
