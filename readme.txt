front end
---------
if you find blank screen add below link in index.php
error_reporting(E_ALL);
1.set yii library path in index.php
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
2.set database connection at /protected/config/main.php
change db details
'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=cartnex_1_0',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'r_',
			'schemaCachingDuration' => 1800,
            'enableProfiling' => false, //for query profiling
            'enableParamLogging' => false, //for query profiling
        ),
3.set url at /protected/config/main.php
change url
config' => array(
            'site_url' => 'http://localhost/cartnex_build_1_0/',//example:http://www.cartnex.org/
            'document_root' => $_SERVER['DOCUMENT_ROOT'] . "/cartnex_build_1_0/",


back end
---------
url/osadmin/
1.set yii library path at /osadmin/index.php
$yii=dirname(__FILE__).'/../../yii/framework/yii.php';
2.set database connection at /osadmin/protected/config/db.php
return array(
    'connectionString' => 'mysql:host=localhost;dbname=cartnex_build_1_0',
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
3.set url at /osadmin/protected/config/params.php
change url
return array(
			'config'=>array(
				'site_url'=>'http://localhost/cartnex_build_1_0/',//example:http://www.cartnex.org/
				'document_root'=>$_SERVER['DOCUMENT_ROOT'].'/cartnex_build_1_0/',//osadmin/',

4.admin login details
url/osadmin
username:demo@cartnex.org
password:demo@123